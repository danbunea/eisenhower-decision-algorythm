(ns eisenhower.eisenhower-controller
  (:require
    [eisenhower.eisenhower-model :refer [model reset-model]]
    [eisenhower.decision-maker :refer [decide]]
    ))


(defn swapm! [x y]
  (swap! y (fn [xx] x)))



(defn init []
  (reset-model ))


(defn define-problem [problem]
  (-> @model
      (assoc :question problem)
      (swapm! model))
  )


(defn define-importance [importance]
  (-> @model
      (assoc :importance importance)
      (swapm! model))
  )

(defn define-urgency [urgency]
  (-> @model
      (assoc :urgency urgency)
      (swapm! model))
  )

(defn define-context [context]
  (-> @model
      (assoc :context context)
      (decide )
      (swapm! model))
  )
