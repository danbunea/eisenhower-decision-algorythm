// Compiled by ClojureScript 1.10.339 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec.alpha');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__20050){
var map__20051 = p__20050;
var map__20051__$1 = ((((!((map__20051 == null)))?(((((map__20051.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20051.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20051):map__20051);
var m = map__20051__$1;
var n = cljs.core.get.call(null,map__20051__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__20051__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var temp__5457__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5457__auto__)){
var ns = temp__5457__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__20053_20075 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__20054_20076 = null;
var count__20055_20077 = (0);
var i__20056_20078 = (0);
while(true){
if((i__20056_20078 < count__20055_20077)){
var f_20079 = cljs.core._nth.call(null,chunk__20054_20076,i__20056_20078);
cljs.core.println.call(null,"  ",f_20079);


var G__20080 = seq__20053_20075;
var G__20081 = chunk__20054_20076;
var G__20082 = count__20055_20077;
var G__20083 = (i__20056_20078 + (1));
seq__20053_20075 = G__20080;
chunk__20054_20076 = G__20081;
count__20055_20077 = G__20082;
i__20056_20078 = G__20083;
continue;
} else {
var temp__5457__auto___20084 = cljs.core.seq.call(null,seq__20053_20075);
if(temp__5457__auto___20084){
var seq__20053_20085__$1 = temp__5457__auto___20084;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__20053_20085__$1)){
var c__4351__auto___20086 = cljs.core.chunk_first.call(null,seq__20053_20085__$1);
var G__20087 = cljs.core.chunk_rest.call(null,seq__20053_20085__$1);
var G__20088 = c__4351__auto___20086;
var G__20089 = cljs.core.count.call(null,c__4351__auto___20086);
var G__20090 = (0);
seq__20053_20075 = G__20087;
chunk__20054_20076 = G__20088;
count__20055_20077 = G__20089;
i__20056_20078 = G__20090;
continue;
} else {
var f_20091 = cljs.core.first.call(null,seq__20053_20085__$1);
cljs.core.println.call(null,"  ",f_20091);


var G__20092 = cljs.core.next.call(null,seq__20053_20085__$1);
var G__20093 = null;
var G__20094 = (0);
var G__20095 = (0);
seq__20053_20075 = G__20092;
chunk__20054_20076 = G__20093;
count__20055_20077 = G__20094;
i__20056_20078 = G__20095;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_20096 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__3949__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_20096);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_20096)))?cljs.core.second.call(null,arglists_20096):arglists_20096));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__20057_20097 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__20058_20098 = null;
var count__20059_20099 = (0);
var i__20060_20100 = (0);
while(true){
if((i__20060_20100 < count__20059_20099)){
var vec__20061_20101 = cljs.core._nth.call(null,chunk__20058_20098,i__20060_20100);
var name_20102 = cljs.core.nth.call(null,vec__20061_20101,(0),null);
var map__20064_20103 = cljs.core.nth.call(null,vec__20061_20101,(1),null);
var map__20064_20104__$1 = ((((!((map__20064_20103 == null)))?(((((map__20064_20103.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20064_20103.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20064_20103):map__20064_20103);
var doc_20105 = cljs.core.get.call(null,map__20064_20104__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_20106 = cljs.core.get.call(null,map__20064_20104__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_20102);

cljs.core.println.call(null," ",arglists_20106);

if(cljs.core.truth_(doc_20105)){
cljs.core.println.call(null," ",doc_20105);
} else {
}


var G__20107 = seq__20057_20097;
var G__20108 = chunk__20058_20098;
var G__20109 = count__20059_20099;
var G__20110 = (i__20060_20100 + (1));
seq__20057_20097 = G__20107;
chunk__20058_20098 = G__20108;
count__20059_20099 = G__20109;
i__20060_20100 = G__20110;
continue;
} else {
var temp__5457__auto___20111 = cljs.core.seq.call(null,seq__20057_20097);
if(temp__5457__auto___20111){
var seq__20057_20112__$1 = temp__5457__auto___20111;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__20057_20112__$1)){
var c__4351__auto___20113 = cljs.core.chunk_first.call(null,seq__20057_20112__$1);
var G__20114 = cljs.core.chunk_rest.call(null,seq__20057_20112__$1);
var G__20115 = c__4351__auto___20113;
var G__20116 = cljs.core.count.call(null,c__4351__auto___20113);
var G__20117 = (0);
seq__20057_20097 = G__20114;
chunk__20058_20098 = G__20115;
count__20059_20099 = G__20116;
i__20060_20100 = G__20117;
continue;
} else {
var vec__20066_20118 = cljs.core.first.call(null,seq__20057_20112__$1);
var name_20119 = cljs.core.nth.call(null,vec__20066_20118,(0),null);
var map__20069_20120 = cljs.core.nth.call(null,vec__20066_20118,(1),null);
var map__20069_20121__$1 = ((((!((map__20069_20120 == null)))?(((((map__20069_20120.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__20069_20120.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__20069_20120):map__20069_20120);
var doc_20122 = cljs.core.get.call(null,map__20069_20121__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_20123 = cljs.core.get.call(null,map__20069_20121__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_20119);

cljs.core.println.call(null," ",arglists_20123);

if(cljs.core.truth_(doc_20122)){
cljs.core.println.call(null," ",doc_20122);
} else {
}


var G__20124 = cljs.core.next.call(null,seq__20057_20112__$1);
var G__20125 = null;
var G__20126 = (0);
var G__20127 = (0);
seq__20057_20097 = G__20124;
chunk__20058_20098 = G__20125;
count__20059_20099 = G__20126;
i__20060_20100 = G__20127;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5457__auto__ = cljs.spec.alpha.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__5457__auto__)){
var fnspec = temp__5457__auto__;
cljs.core.print.call(null,"Spec");

var seq__20071 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__20072 = null;
var count__20073 = (0);
var i__20074 = (0);
while(true){
if((i__20074 < count__20073)){
var role = cljs.core._nth.call(null,chunk__20072,i__20074);
var temp__5457__auto___20128__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___20128__$1)){
var spec_20129 = temp__5457__auto___20128__$1;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_20129));
} else {
}


var G__20130 = seq__20071;
var G__20131 = chunk__20072;
var G__20132 = count__20073;
var G__20133 = (i__20074 + (1));
seq__20071 = G__20130;
chunk__20072 = G__20131;
count__20073 = G__20132;
i__20074 = G__20133;
continue;
} else {
var temp__5457__auto____$1 = cljs.core.seq.call(null,seq__20071);
if(temp__5457__auto____$1){
var seq__20071__$1 = temp__5457__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__20071__$1)){
var c__4351__auto__ = cljs.core.chunk_first.call(null,seq__20071__$1);
var G__20134 = cljs.core.chunk_rest.call(null,seq__20071__$1);
var G__20135 = c__4351__auto__;
var G__20136 = cljs.core.count.call(null,c__4351__auto__);
var G__20137 = (0);
seq__20071 = G__20134;
chunk__20072 = G__20135;
count__20073 = G__20136;
i__20074 = G__20137;
continue;
} else {
var role = cljs.core.first.call(null,seq__20071__$1);
var temp__5457__auto___20138__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__5457__auto___20138__$2)){
var spec_20139 = temp__5457__auto___20138__$2;
cljs.core.print.call(null,["\n ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.name.call(null,role)),":"].join(''),cljs.spec.alpha.describe.call(null,spec_20139));
} else {
}


var G__20140 = cljs.core.next.call(null,seq__20071__$1);
var G__20141 = null;
var G__20142 = (0);
var G__20143 = (0);
seq__20071 = G__20140;
chunk__20072 = G__20141;
count__20073 = G__20142;
i__20074 = G__20143;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map
