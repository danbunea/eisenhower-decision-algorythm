// Compiled by ClojureScript 1.10.339 {}
goog.provide('eisenhower.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('eisenhower.eisenhower_controller');
goog.require('eisenhower.eisenhower_model');
goog.require('eisenhower.eisenhower_views');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core.println.call(null,"This text is printed from src/cljs-kata/core.cljs. Go ahead and edit it and see reloading in action.");
if((typeof eisenhower !== 'undefined') && (typeof eisenhower.core !== 'undefined') && (typeof eisenhower.core.init !== 'undefined')){
} else {
eisenhower.core.init = (function (){
cljs.core.print.call(null,"initializing...");

eisenhower.eisenhower_controller.init.call(null);

return cljs.core.print.call(null,"initialized");
})()
;
}
eisenhower.core.application = (function eisenhower$core$application(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [eisenhower.eisenhower_views.screen_component,cljs.core.deref.call(null,eisenhower.eisenhower_model.model)], null);
});
reagent.core.render_component.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [eisenhower.core.application], null),document.getElementById("app"));
eisenhower.core.on_js_reload = (function eisenhower$core$on_js_reload(){
return null;
});

//# sourceMappingURL=core.js.map
