(ns  eisenhower.decision-maker
  )

(defn decide [what]
  (let [decision (cond
                   (and
                     (>= (:importance what) 5)
                     (< (:urgency what) 5))
                   "Decide when you will do it"

                   (and
                     (>= (:importance what) 5)
                     (>= (:urgency what) 5))
                   "Do it immediately"

                   (and
                     (< (:importance what) 5)
                     (>= (:urgency what) 5))
                   "Delegate to somebody else"
                   :else
                   "Do it later")]

    (assoc what :decision decision)))
