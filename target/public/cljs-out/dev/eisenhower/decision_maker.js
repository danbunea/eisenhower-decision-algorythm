// Compiled by ClojureScript 1.10.339 {}
goog.provide('eisenhower.decision_maker');
goog.require('cljs.core');
eisenhower.decision_maker.decide = (function eisenhower$decision_maker$decide(what){
var decision = (((((new cljs.core.Keyword(null,"importance","importance",-557670881).cljs$core$IFn$_invoke$arity$1(what) >= (5))) && ((new cljs.core.Keyword(null,"urgency","urgency",-1292117426).cljs$core$IFn$_invoke$arity$1(what) < (5)))))?"Decide when you will do it":(((((new cljs.core.Keyword(null,"importance","importance",-557670881).cljs$core$IFn$_invoke$arity$1(what) >= (5))) && ((new cljs.core.Keyword(null,"urgency","urgency",-1292117426).cljs$core$IFn$_invoke$arity$1(what) >= (5)))))?"Do it immediately":(((((new cljs.core.Keyword(null,"importance","importance",-557670881).cljs$core$IFn$_invoke$arity$1(what) < (5))) && ((new cljs.core.Keyword(null,"urgency","urgency",-1292117426).cljs$core$IFn$_invoke$arity$1(what) >= (5)))))?"Delegate to somebody else":"Do it later"
)));
return cljs.core.assoc.call(null,what,new cljs.core.Keyword(null,"decision","decision",820953053),decision);
});

//# sourceMappingURL=decision_maker.js.map
