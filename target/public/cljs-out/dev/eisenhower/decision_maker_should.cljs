(ns eisenhower.decision-maker-should
  (:require
    [cljs.test :refer-macros [deftest is testing]]
    [eisenhower.decision-maker :refer [decide]]
    ))





(defn expect-decision [expected-decision ent]
  (is (= (assoc ent :decision expected-decision) (decide ent)))
  )


(deftest place-a-decision-in-the-matrix
  (testing "return-decide-when-important-not-urgent"
    (expect-decision
      "Decide when you will do it"
      (-> {}
          (assoc :question "Can I?")
          (assoc :importance 6)
          (assoc :urgency 2)
          (assoc :context "")))
    )
  (testing "return-delegate-when-not-important-but-urgent"
    (expect-decision
      "Delegate to somebody else"
      (-> {}
          (assoc :question "Can I?")
          (assoc :importance 2)
          (assoc :urgency 6)
          (assoc :context "")))
    )

  (testing "return-do-it-when-important-and-urgent"
    (expect-decision
      "Do it immediately"
      (-> {}
          (assoc :question "Can I?")
          (assoc :importance 7)
          (assoc :urgency 6)
          (assoc :context "")))
    )
  (testing "return-do-it-later-when-not-important-and-not-urgent"
    (expect-decision
      "Do it later"
      (-> {}
          (assoc :question "Can I?")
          (assoc :importance 1)
          (assoc :urgency 2)
          (assoc :context "")))
    )
  )
