// Compiled by ClojureScript 1.10.339 {}
goog.provide('eisenhower.eisenhower_controller');
goog.require('cljs.core');
goog.require('eisenhower.eisenhower_model');
goog.require('eisenhower.decision_maker');
eisenhower.eisenhower_controller.swapm_BANG_ = (function eisenhower$eisenhower_controller$swapm_BANG_(x,y){
return cljs.core.swap_BANG_.call(null,y,(function (xx){
return x;
}));
});
eisenhower.eisenhower_controller.init = (function eisenhower$eisenhower_controller$init(){
return eisenhower.eisenhower_model.reset_model.call(null);
});
eisenhower.eisenhower_controller.define_problem = (function eisenhower$eisenhower_controller$define_problem(problem){
return eisenhower.eisenhower_controller.swapm_BANG_.call(null,cljs.core.assoc.call(null,cljs.core.deref.call(null,eisenhower.eisenhower_model.model),new cljs.core.Keyword(null,"question","question",-1411720117),problem),eisenhower.eisenhower_model.model);
});
eisenhower.eisenhower_controller.define_importance = (function eisenhower$eisenhower_controller$define_importance(importance){
return eisenhower.eisenhower_controller.swapm_BANG_.call(null,cljs.core.assoc.call(null,cljs.core.deref.call(null,eisenhower.eisenhower_model.model),new cljs.core.Keyword(null,"importance","importance",-557670881),importance),eisenhower.eisenhower_model.model);
});
eisenhower.eisenhower_controller.define_urgency = (function eisenhower$eisenhower_controller$define_urgency(urgency){
return eisenhower.eisenhower_controller.swapm_BANG_.call(null,cljs.core.assoc.call(null,cljs.core.deref.call(null,eisenhower.eisenhower_model.model),new cljs.core.Keyword(null,"urgency","urgency",-1292117426),urgency),eisenhower.eisenhower_model.model);
});
eisenhower.eisenhower_controller.define_context = (function eisenhower$eisenhower_controller$define_context(context){
return eisenhower.eisenhower_controller.swapm_BANG_.call(null,eisenhower.decision_maker.decide.call(null,cljs.core.assoc.call(null,cljs.core.deref.call(null,eisenhower.eisenhower_model.model),new cljs.core.Keyword(null,"context","context",-830191113),context)),eisenhower.eisenhower_model.model);
});

//# sourceMappingURL=eisenhower_controller.js.map
