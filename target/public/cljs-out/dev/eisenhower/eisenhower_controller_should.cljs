(ns eisenhower.eisenhower-controller-should
  (:require
    [cljs.test :refer-macros [deftest is testing]]
    [eisenhower.eisenhower-controller :as controller]
    [eisenhower.eisenhower-model :refer [model]]
    ))



(deftest initialize
  (is (= {} (controller/init )))
  )



(deftest define-problem
  (controller/define-problem "Can I?")
  (is (= "Can I?" (:question @model))))



(deftest define-importance
  (controller/define-importance 6)
  (is (= 6 (:importance @model))))




(deftest define-urgency
  (controller/define-urgency 2)
  (is (= 2 (:urgency @model))))


(deftest define-context
  (controller/define-context "2 hours")
  (is (= "2 hours" (:context @model)))
  (is (= "Decide when you will do it" (:decision @model)))
  )
