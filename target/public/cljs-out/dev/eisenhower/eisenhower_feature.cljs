(ns eisenhower.eisenhower-feature
  (:require
    [cljs.test :refer-macros [deftest is testing]]
    [eisenhower.eisenhower-controller :as controller ]
    [eisenhower.eisenhower-model :refer [model reset-model] ]
    ))





(deftest lead-step-by-step-to-a-decision
  (let [
         model-1 (controller/init )
         model-2 (controller/define-problem "Can I?" )
         model-3 (controller/define-importance 6 )
         model-4 (controller/define-urgency 2 )
         after-model (controller/define-context "" )]
  (is (= "Decide when you will do it" (:decision after-model)))))
