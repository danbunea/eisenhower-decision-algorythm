// Compiled by ClojureScript 1.10.339 {}
goog.provide('eisenhower.eisenhower_feature');
goog.require('cljs.core');
goog.require('cljs.test');
goog.require('eisenhower.eisenhower_controller');
goog.require('eisenhower.eisenhower_model');
eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision = (function eisenhower$eisenhower_feature$lead_step_by_step_to_a_decision(){
return cljs.test.test_var.call(null,eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision.cljs$lang$var);
});
eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision.cljs$lang$test = (function (){
var model_1 = eisenhower.eisenhower_controller.init.call(null);
var model_2 = eisenhower.eisenhower_controller.define_problem.call(null,"Can I?");
var model_3 = eisenhower.eisenhower_controller.define_importance.call(null,(6));
var model_4 = eisenhower.eisenhower_controller.define_urgency.call(null,(2));
var after_model = eisenhower.eisenhower_controller.define_context.call(null,"");
try{var values__8220__auto__ = (new cljs.core.List(null,"Decide when you will do it",(new cljs.core.List(null,new cljs.core.Keyword(null,"decision","decision",820953053).cljs$core$IFn$_invoke$arity$1(after_model),null,(1),null)),(2),null));
var result__8221__auto__ = cljs.core.apply.call(null,cljs.core._EQ_,values__8220__auto__);
if(cljs.core.truth_(result__8221__auto__)){
cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"pass","pass",1574159993),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),"Decide when you will do it",cljs.core.list(new cljs.core.Keyword(null,"decision","decision",820953053),new cljs.core.Symbol(null,"after-model","after-model",-42967195,null))),new cljs.core.Keyword(null,"actual","actual",107306363),cljs.core.cons.call(null,cljs.core._EQ_,values__8220__auto__),new cljs.core.Keyword(null,"message","message",-406056002),null], null));
} else {
cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"fail","fail",1706214930),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),"Decide when you will do it",cljs.core.list(new cljs.core.Keyword(null,"decision","decision",820953053),new cljs.core.Symbol(null,"after-model","after-model",-42967195,null))),new cljs.core.Keyword(null,"actual","actual",107306363),(new cljs.core.List(null,new cljs.core.Symbol(null,"not","not",1044554643,null),(new cljs.core.List(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"=","=",-1501502141,null),values__8220__auto__),null,(1),null)),(2),null)),new cljs.core.Keyword(null,"message","message",-406056002),null], null));
}

return result__8221__auto__;
}catch (e14175){var t__8265__auto__ = e14175;
return cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"error","error",-978969032),new cljs.core.Keyword(null,"expected","expected",1583670997),cljs.core.list(new cljs.core.Symbol(null,"=","=",-1501502141,null),"Decide when you will do it",cljs.core.list(new cljs.core.Keyword(null,"decision","decision",820953053),new cljs.core.Symbol(null,"after-model","after-model",-42967195,null))),new cljs.core.Keyword(null,"actual","actual",107306363),t__8265__auto__,new cljs.core.Keyword(null,"message","message",-406056002),null], null));
}});

eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision.cljs$lang$var = new cljs.core.Var(function(){return eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision;},new cljs.core.Symbol("eisenhower.eisenhower-feature","lead-step-by-step-to-a-decision","eisenhower.eisenhower-feature/lead-step-by-step-to-a-decision",-929055512,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-feature","eisenhower.eisenhower-feature",-1977765587,null),new cljs.core.Symbol(null,"lead-step-by-step-to-a-decision","lead-step-by-step-to-a-decision",-298417487,null),"test/eisenhower/eisenhower_feature.cljs",41,1,12,12,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision)?eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision.cljs$lang$test:null)]));

//# sourceMappingURL=eisenhower_feature.js.map
