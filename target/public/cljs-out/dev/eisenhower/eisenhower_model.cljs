(ns eisenhower.eisenhower-model
  (:require  [reagent.core :as r :refer [atom]]))

(defonce model (r/atom {}))

(defn reset-model []
  (reset! model {}))
