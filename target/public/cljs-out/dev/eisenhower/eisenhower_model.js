// Compiled by ClojureScript 1.10.339 {}
goog.provide('eisenhower.eisenhower_model');
goog.require('cljs.core');
goog.require('reagent.core');
if((typeof eisenhower !== 'undefined') && (typeof eisenhower.eisenhower_model !== 'undefined') && (typeof eisenhower.eisenhower_model.model !== 'undefined')){
} else {
eisenhower.eisenhower_model.model = reagent.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
eisenhower.eisenhower_model.reset_model = (function eisenhower$eisenhower_model$reset_model(){
return cljs.core.reset_BANG_.call(null,eisenhower.eisenhower_model.model,cljs.core.PersistentArrayMap.EMPTY);
});

//# sourceMappingURL=eisenhower_model.js.map
