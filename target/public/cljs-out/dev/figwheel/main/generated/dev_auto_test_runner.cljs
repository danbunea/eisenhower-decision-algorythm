(ns figwheel.main.generated.dev-auto-test-runner
  (:require [cljs.test :refer-macros [run-tests]]
            [cljs-test-display.core] [eisenhower.eisenhower-feature] [eisenhower.decision-maker-should] [eisenhower.eisenhower-controller-should]))

  (run-tests (cljs-test-display.core/init! "app-auto-testing") (quote eisenhower.eisenhower-feature) (quote eisenhower.decision-maker-should) (quote eisenhower.eisenhower-controller-should))