// Compiled by ClojureScript 1.10.339 {}
goog.provide('figwheel.main.generated.dev_auto_test_runner');
goog.require('cljs.core');
goog.require('cljs.test');
goog.require('cljs_test_display.core');
goog.require('eisenhower.eisenhower_feature');
goog.require('eisenhower.decision_maker_should');
goog.require('eisenhower.eisenhower_controller_should');
cljs.test.run_block.call(null,(function (){var env14189 = cljs_test_display.core.init_BANG_.call(null,"app-auto-testing");
var summary14190 = cljs.core.volatile_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"summary","summary",380847952),new cljs.core.Keyword(null,"fail","fail",1706214930),(0),new cljs.core.Keyword(null,"error","error",-978969032),(0),new cljs.core.Keyword(null,"pass","pass",1574159993),(0),new cljs.core.Keyword(null,"test","test",577538877),(0)], null));
return cljs.core.concat.call(null,cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
cljs.test.set_env_BANG_.call(null,env14189);

cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.eisenhower-feature","eisenhower.eisenhower-feature",-1977765587,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"begin-test-ns","begin-test-ns",-1701237033)], null));

return cljs.test.block.call(null,(function (){var env__8333__auto__ = cljs.test.get_current_env.call(null);
return cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
cljs.test.set_env_BANG_.call(null,cljs.test.empty_env.call(null));
} else {
}


return null;
});})(env__8333__auto__,env14189,summary14190))
], null),cljs.test.test_vars_block.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Var(function(){return eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision;},new cljs.core.Symbol("eisenhower.eisenhower-feature","lead-step-by-step-to-a-decision","eisenhower.eisenhower-feature/lead-step-by-step-to-a-decision",-929055512,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-feature","eisenhower.eisenhower-feature",-1977765587,null),new cljs.core.Symbol(null,"lead-step-by-step-to-a-decision","lead-step-by-step-to-a-decision",-298417487,null),"test/eisenhower/eisenhower_feature.cljs",41,1,12,12,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision)?eisenhower.eisenhower_feature.lead_step_by_step_to_a_decision.cljs$lang$test:null)]))], null)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
return cljs.test.clear_env_BANG_.call(null);
} else {
return null;
}
});})(env__8333__auto__,env14189,summary14190))
], null));
})());
});})(env14189,summary14190))
,((function (env14189,summary14190){
return (function (){
return cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.eisenhower-feature","eisenhower.eisenhower-feature",-1977765587,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"end-test-ns","end-test-ns",1620675645)], null));
});})(env14189,summary14190))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
return cljs.core._vreset_BANG_.call(null,summary14190,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core._PLUS_).call(null,cljs.core._deref.call(null,summary14190),new cljs.core.Keyword(null,"report-counters","report-counters",-1702609242).cljs$core$IFn$_invoke$arity$1(cljs.test.get_and_clear_env_BANG_.call(null))));
});})(env14189,summary14190))
], null)),cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
cljs.test.set_env_BANG_.call(null,env14189);

cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.decision-maker-should","eisenhower.decision-maker-should",-429237546,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"begin-test-ns","begin-test-ns",-1701237033)], null));

return cljs.test.block.call(null,(function (){var env__8333__auto__ = cljs.test.get_current_env.call(null);
return cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
cljs.test.set_env_BANG_.call(null,cljs.test.empty_env.call(null));
} else {
}


return null;
});})(env__8333__auto__,env14189,summary14190))
], null),cljs.test.test_vars_block.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Var(function(){return eisenhower.decision_maker_should.place_a_decision_in_the_matrix;},new cljs.core.Symbol("eisenhower.decision-maker-should","place-a-decision-in-the-matrix","eisenhower.decision-maker-should/place-a-decision-in-the-matrix",489799159,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.decision-maker-should","eisenhower.decision-maker-should",-429237546,null),new cljs.core.Symbol(null,"place-a-decision-in-the-matrix","place-a-decision-in-the-matrix",-221669749,null),"test/eisenhower/decision_maker_should.cljs",(40),(1),(16),(16),cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.decision_maker_should.place_a_decision_in_the_matrix)?eisenhower.decision_maker_should.place_a_decision_in_the_matrix.cljs$lang$test:null)]))], null)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
return cljs.test.clear_env_BANG_.call(null);
} else {
return null;
}
});})(env__8333__auto__,env14189,summary14190))
], null));
})());
});})(env14189,summary14190))
,((function (env14189,summary14190){
return (function (){
return cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.decision-maker-should","eisenhower.decision-maker-should",-429237546,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"end-test-ns","end-test-ns",1620675645)], null));
});})(env14189,summary14190))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
return cljs.core._vreset_BANG_.call(null,summary14190,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core._PLUS_).call(null,cljs.core._deref.call(null,summary14190),new cljs.core.Keyword(null,"report-counters","report-counters",-1702609242).cljs$core$IFn$_invoke$arity$1(cljs.test.get_and_clear_env_BANG_.call(null))));
});})(env14189,summary14190))
], null)),cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
cljs.test.set_env_BANG_.call(null,env14189);

cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"begin-test-ns","begin-test-ns",-1701237033)], null));

return cljs.test.block.call(null,(function (){var env__8333__auto__ = cljs.test.get_current_env.call(null);
return cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
cljs.test.set_env_BANG_.call(null,cljs.test.empty_env.call(null));
} else {
}


return null;
});})(env__8333__auto__,env14189,summary14190))
], null),cljs.test.test_vars_block.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Var(function(){return eisenhower.eisenhower_controller_should.initialize;},new cljs.core.Symbol("eisenhower.eisenhower-controller-should","initialize","eisenhower.eisenhower-controller-should/initialize",628407379,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Symbol(null,"initialize","initialize",-2044482856,null),"test/eisenhower/eisenhower_controller_should.cljs",20,1,10,10,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_controller_should.initialize)?eisenhower.eisenhower_controller_should.initialize.cljs$lang$test:null)])),new cljs.core.Var(function(){return eisenhower.eisenhower_controller_should.define_problem;},new cljs.core.Symbol("eisenhower.eisenhower-controller-should","define-problem","eisenhower.eisenhower-controller-should/define-problem",-755288676,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Symbol(null,"define-problem","define-problem",2109744155,null),"test/eisenhower/eisenhower_controller_should.cljs",24,1,16,16,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_controller_should.define_problem)?eisenhower.eisenhower_controller_should.define_problem.cljs$lang$test:null)])),new cljs.core.Var(function(){return eisenhower.eisenhower_controller_should.define_importance;},new cljs.core.Symbol("eisenhower.eisenhower-controller-should","define-importance","eisenhower.eisenhower-controller-should/define-importance",-851193497,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Symbol(null,"define-importance","define-importance",1756816108,null),"test/eisenhower/eisenhower_controller_should.cljs",27,1,22,22,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_controller_should.define_importance)?eisenhower.eisenhower_controller_should.define_importance.cljs$lang$test:null)])),new cljs.core.Var(function(){return eisenhower.eisenhower_controller_should.define_urgency;},new cljs.core.Symbol("eisenhower.eisenhower-controller-should","define-urgency","eisenhower.eisenhower-controller-should/define-urgency",-602608317,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Symbol(null,"define-urgency","define-urgency",-1086254138,null),"test/eisenhower/eisenhower_controller_should.cljs",24,1,29,29,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_controller_should.define_urgency)?eisenhower.eisenhower_controller_should.define_urgency.cljs$lang$test:null)])),new cljs.core.Var(function(){return eisenhower.eisenhower_controller_should.define_context;},new cljs.core.Symbol("eisenhower.eisenhower-controller-should","define-context","eisenhower.eisenhower-controller-should/define-context",-1770575079,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Symbol(null,"define-context","define-context",-117286506,null),"test/eisenhower/eisenhower_controller_should.cljs",24,1,34,34,cljs.core.List.EMPTY,null,(cljs.core.truth_(eisenhower.eisenhower_controller_should.define_context)?eisenhower.eisenhower_controller_should.define_context.cljs$lang$test:null)]))], null)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env__8333__auto__,env14189,summary14190){
return (function (){
if((env__8333__auto__ == null)){
return cljs.test.clear_env_BANG_.call(null);
} else {
return null;
}
});})(env__8333__auto__,env14189,summary14190))
], null));
})());
});})(env14189,summary14190))
,((function (env14189,summary14190){
return (function (){
return cljs.test.do_report.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Symbol(null,"eisenhower.eisenhower-controller-should","eisenhower.eisenhower-controller-should",886668825,null),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"end-test-ns","end-test-ns",1620675645)], null));
});})(env14189,summary14190))
], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
return cljs.core._vreset_BANG_.call(null,summary14190,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core._PLUS_).call(null,cljs.core._deref.call(null,summary14190),new cljs.core.Keyword(null,"report-counters","report-counters",-1702609242).cljs$core$IFn$_invoke$arity$1(cljs.test.get_and_clear_env_BANG_.call(null))));
});})(env14189,summary14190))
], null)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [((function (env14189,summary14190){
return (function (){
cljs.test.set_env_BANG_.call(null,env14189);

cljs.test.do_report.call(null,cljs.core.deref.call(null,summary14190));

cljs.test.report.call(null,cljs.core.assoc.call(null,cljs.core.deref.call(null,summary14190),new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"end-run-tests","end-run-tests",267300563)));

return cljs.test.clear_env_BANG_.call(null);
});})(env14189,summary14190))
], null));
})());

//# sourceMappingURL=dev_auto_test_runner.js.map
