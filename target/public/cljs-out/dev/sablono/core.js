// Compiled by ClojureScript 1.10.339 {}
goog.provide('sablono.core');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('goog.string');
goog.require('sablono.normalize');
goog.require('sablono.util');
goog.require('sablono.interpreter');
goog.require('goog.dom');
/**
 * Add an optional attribute argument to a function that returns a element vector.
 */
sablono.core.wrap_attrs = (function sablono$core$wrap_attrs(func){
return (function() { 
var G__11620__delegate = function (args){
if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,args))){
var vec__11617 = cljs.core.apply.call(null,func,cljs.core.rest.call(null,args));
var seq__11618 = cljs.core.seq.call(null,vec__11617);
var first__11619 = cljs.core.first.call(null,seq__11618);
var seq__11618__$1 = cljs.core.next.call(null,seq__11618);
var tag = first__11619;
var body = seq__11618__$1;
if(cljs.core.map_QMARK_.call(null,cljs.core.first.call(null,body))){
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.merge.call(null,cljs.core.first.call(null,body),cljs.core.first.call(null,args))], null),cljs.core.rest.call(null,body));
} else {
return cljs.core.into.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tag,cljs.core.first.call(null,args)], null),body);
}
} else {
return cljs.core.apply.call(null,func,args);
}
};
var G__11620 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__11621__i = 0, G__11621__a = new Array(arguments.length -  0);
while (G__11621__i < G__11621__a.length) {G__11621__a[G__11621__i] = arguments[G__11621__i + 0]; ++G__11621__i;}
  args = new cljs.core.IndexedSeq(G__11621__a,0,null);
} 
return G__11620__delegate.call(this,args);};
G__11620.cljs$lang$maxFixedArity = 0;
G__11620.cljs$lang$applyTo = (function (arglist__11622){
var args = cljs.core.seq(arglist__11622);
return G__11620__delegate(args);
});
G__11620.cljs$core$IFn$_invoke$arity$variadic = G__11620__delegate;
return G__11620;
})()
;
});
sablono.core.update_arglists = (function sablono$core$update_arglists(arglists){
var iter__4324__auto__ = (function sablono$core$update_arglists_$_iter__11623(s__11624){
return (new cljs.core.LazySeq(null,(function (){
var s__11624__$1 = s__11624;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__11624__$1);
if(temp__5457__auto__){
var s__11624__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__11624__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__11624__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__11626 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__11625 = (0);
while(true){
if((i__11625 < size__4323__auto__)){
var args = cljs.core._nth.call(null,c__4322__auto__,i__11625);
cljs.core.chunk_append.call(null,b__11626,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)));

var G__11627 = (i__11625 + (1));
i__11625 = G__11627;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11626),sablono$core$update_arglists_$_iter__11623.call(null,cljs.core.chunk_rest.call(null,s__11624__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11626),null);
}
} else {
var args = cljs.core.first.call(null,s__11624__$2);
return cljs.core.cons.call(null,cljs.core.vec.call(null,cljs.core.cons.call(null,new cljs.core.Symbol(null,"attr-map?","attr-map?",116307443,null),args)),sablono$core$update_arglists_$_iter__11623.call(null,cljs.core.rest.call(null,s__11624__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,arglists);
});
/**
 * Include a list of external stylesheet files.
 */
sablono.core.include_css = (function sablono$core$include_css(var_args){
var args__4534__auto__ = [];
var len__4531__auto___11633 = arguments.length;
var i__4532__auto___11634 = (0);
while(true){
if((i__4532__auto___11634 < len__4531__auto___11633)){
args__4534__auto__.push((arguments[i__4532__auto___11634]));

var G__11635 = (i__4532__auto___11634 + (1));
i__4532__auto___11634 = G__11635;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((0) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((0)),(0),null)):null);
return sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic(argseq__4535__auto__);
});

sablono.core.include_css.cljs$core$IFn$_invoke$arity$variadic = (function (styles){
var iter__4324__auto__ = (function sablono$core$iter__11629(s__11630){
return (new cljs.core.LazySeq(null,(function (){
var s__11630__$1 = s__11630;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__11630__$1);
if(temp__5457__auto__){
var s__11630__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__11630__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__11630__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__11632 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__11631 = (0);
while(true){
if((i__11631 < size__4323__auto__)){
var style = cljs.core._nth.call(null,c__4322__auto__,i__11631);
cljs.core.chunk_append.call(null,b__11632,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null));

var G__11636 = (i__11631 + (1));
i__11631 = G__11636;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11632),sablono$core$iter__11629.call(null,cljs.core.chunk_rest.call(null,s__11630__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11632),null);
}
} else {
var style = cljs.core.first.call(null,s__11630__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"link","link",-1769163468),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"text/css",new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,style),new cljs.core.Keyword(null,"rel","rel",1378823488),"stylesheet"], null)], null),sablono$core$iter__11629.call(null,cljs.core.rest.call(null,s__11630__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,styles);
});

sablono.core.include_css.cljs$lang$maxFixedArity = (0);

/** @this {Function} */
sablono.core.include_css.cljs$lang$applyTo = (function (seq11628){
var self__4519__auto__ = this;
return self__4519__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq11628));
});

/**
 * Include the JavaScript library at `src`.
 */
sablono.core.include_js = (function sablono$core$include_js(src){
return goog.dom.appendChild(goog.dom.getDocument().body,goog.dom.createDom("script",({"src": src})));
});
/**
 * Include Facebook's React JavaScript library.
 */
sablono.core.include_react = (function sablono$core$include_react(){
return sablono.core.include_js.call(null,"http://fb.me/react-0.12.2.js");
});
/**
 * Wraps some content in a HTML hyperlink with the supplied URL.
 */
sablono.core.link_to11637 = (function sablono$core$link_to11637(var_args){
var args__4534__auto__ = [];
var len__4531__auto___11640 = arguments.length;
var i__4532__auto___11641 = (0);
while(true){
if((i__4532__auto___11641 < len__4531__auto___11640)){
args__4534__auto__.push((arguments[i__4532__auto___11641]));

var G__11642 = (i__4532__auto___11641 + (1));
i__4532__auto___11641 = G__11642;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return sablono.core.link_to11637.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

sablono.core.link_to11637.cljs$core$IFn$_invoke$arity$variadic = (function (url,content){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),sablono.util.as_str.call(null,url)], null),content], null);
});

sablono.core.link_to11637.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.link_to11637.cljs$lang$applyTo = (function (seq11638){
var G__11639 = cljs.core.first.call(null,seq11638);
var seq11638__$1 = cljs.core.next.call(null,seq11638);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__11639,seq11638__$1);
});


sablono.core.link_to = sablono.core.wrap_attrs.call(null,sablono.core.link_to11637);
/**
 * Wraps some content in a HTML hyperlink with the supplied e-mail
 *   address. If no content provided use the e-mail address as content.
 */
sablono.core.mail_to11643 = (function sablono$core$mail_to11643(var_args){
var args__4534__auto__ = [];
var len__4531__auto___11650 = arguments.length;
var i__4532__auto___11651 = (0);
while(true){
if((i__4532__auto___11651 < len__4531__auto___11650)){
args__4534__auto__.push((arguments[i__4532__auto___11651]));

var G__11652 = (i__4532__auto___11651 + (1));
i__4532__auto___11651 = G__11652;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return sablono.core.mail_to11643.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

sablono.core.mail_to11643.cljs$core$IFn$_invoke$arity$variadic = (function (e_mail,p__11646){
var vec__11647 = p__11646;
var content = cljs.core.nth.call(null,vec__11647,(0),null);
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),["mailto:",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_mail)].join('')], null),(function (){var or__3949__auto__ = content;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return e_mail;
}
})()], null);
});

sablono.core.mail_to11643.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.mail_to11643.cljs$lang$applyTo = (function (seq11644){
var G__11645 = cljs.core.first.call(null,seq11644);
var seq11644__$1 = cljs.core.next.call(null,seq11644);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__11645,seq11644__$1);
});


sablono.core.mail_to = sablono.core.wrap_attrs.call(null,sablono.core.mail_to11643);
/**
 * Wrap a collection in an unordered list.
 */
sablono.core.unordered_list11653 = (function sablono$core$unordered_list11653(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),(function (){var iter__4324__auto__ = (function sablono$core$unordered_list11653_$_iter__11654(s__11655){
return (new cljs.core.LazySeq(null,(function (){
var s__11655__$1 = s__11655;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__11655__$1);
if(temp__5457__auto__){
var s__11655__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__11655__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__11655__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__11657 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__11656 = (0);
while(true){
if((i__11656 < size__4323__auto__)){
var x = cljs.core._nth.call(null,c__4322__auto__,i__11656);
cljs.core.chunk_append.call(null,b__11657,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__11658 = (i__11656 + (1));
i__11656 = G__11658;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11657),sablono$core$unordered_list11653_$_iter__11654.call(null,cljs.core.chunk_rest.call(null,s__11655__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11657),null);
}
} else {
var x = cljs.core.first.call(null,s__11655__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$unordered_list11653_$_iter__11654.call(null,cljs.core.rest.call(null,s__11655__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,coll);
})()], null);
});

sablono.core.unordered_list = sablono.core.wrap_attrs.call(null,sablono.core.unordered_list11653);
/**
 * Wrap a collection in an ordered list.
 */
sablono.core.ordered_list11659 = (function sablono$core$ordered_list11659(coll){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol","ol",932524051),(function (){var iter__4324__auto__ = (function sablono$core$ordered_list11659_$_iter__11660(s__11661){
return (new cljs.core.LazySeq(null,(function (){
var s__11661__$1 = s__11661;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__11661__$1);
if(temp__5457__auto__){
var s__11661__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__11661__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__11661__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__11663 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__11662 = (0);
while(true){
if((i__11662 < size__4323__auto__)){
var x = cljs.core._nth.call(null,c__4322__auto__,i__11662);
cljs.core.chunk_append.call(null,b__11663,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null));

var G__11664 = (i__11662 + (1));
i__11662 = G__11664;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11663),sablono$core$ordered_list11659_$_iter__11660.call(null,cljs.core.chunk_rest.call(null,s__11661__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11663),null);
}
} else {
var x = cljs.core.first.call(null,s__11661__$2);
return cljs.core.cons.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),x], null),sablono$core$ordered_list11659_$_iter__11660.call(null,cljs.core.rest.call(null,s__11661__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,coll);
})()], null);
});

sablono.core.ordered_list = sablono.core.wrap_attrs.call(null,sablono.core.ordered_list11659);
/**
 * Create an image element.
 */
sablono.core.image11665 = (function sablono$core$image11665(var_args){
var G__11667 = arguments.length;
switch (G__11667) {
case 1:
return sablono.core.image11665.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.image11665.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.image11665.cljs$core$IFn$_invoke$arity$1 = (function (src){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.call(null,src)], null)], null);
});

sablono.core.image11665.cljs$core$IFn$_invoke$arity$2 = (function (src,alt){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),sablono.util.as_str.call(null,src),new cljs.core.Keyword(null,"alt","alt",-3214426),alt], null)], null);
});

sablono.core.image11665.cljs$lang$maxFixedArity = 2;


sablono.core.image = sablono.core.wrap_attrs.call(null,sablono.core.image11665);
sablono.core._STAR_group_STAR_ = cljs.core.PersistentVector.EMPTY;
/**
 * Create a field name from the supplied argument the current field group.
 */
sablono.core.make_name = (function sablono$core$make_name(name){
return cljs.core.reduce.call(null,(function (p1__11669_SHARP_,p2__11670_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11669_SHARP_),"[",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11670_SHARP_),"]"].join('');
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
 * Create a field id from the supplied argument and current field group.
 */
sablono.core.make_id = (function sablono$core$make_id(name){
return cljs.core.reduce.call(null,(function (p1__11671_SHARP_,p2__11672_SHARP_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__11671_SHARP_),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p2__11672_SHARP_)].join('');
}),cljs.core.conj.call(null,sablono.core._STAR_group_STAR_,sablono.util.as_str.call(null,name)));
});
/**
 * Creates a new <input> element.
 */
sablono.core.input_field_STAR_ = (function sablono$core$input_field_STAR_(var_args){
var G__11674 = arguments.length;
switch (G__11674) {
case 2:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (type,name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.input_field_STAR_.cljs$core$IFn$_invoke$arity$3 = (function (type,name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),type,new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__3949__auto__ = value;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return undefined;
}
})()], null)], null);
});

sablono.core.input_field_STAR_.cljs$lang$maxFixedArity = 3;

/**
 * Creates a color input field.
 */
sablono.core.color_field11676 = (function sablono$core$color_field11676(var_args){
var G__11678 = arguments.length;
switch (G__11678) {
case 1:
return sablono.core.color_field11676.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.color_field11676.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.color_field11676.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null))].join(''),name__11607__auto__);
});

sablono.core.color_field11676.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"color","color",-1642760596,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.color_field11676.cljs$lang$maxFixedArity = 2;


sablono.core.color_field = sablono.core.wrap_attrs.call(null,sablono.core.color_field11676);

/**
 * Creates a date input field.
 */
sablono.core.date_field11679 = (function sablono$core$date_field11679(var_args){
var G__11681 = arguments.length;
switch (G__11681) {
case 1:
return sablono.core.date_field11679.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.date_field11679.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.date_field11679.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null))].join(''),name__11607__auto__);
});

sablono.core.date_field11679.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"date","date",177097065,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.date_field11679.cljs$lang$maxFixedArity = 2;


sablono.core.date_field = sablono.core.wrap_attrs.call(null,sablono.core.date_field11679);

/**
 * Creates a datetime input field.
 */
sablono.core.datetime_field11682 = (function sablono$core$datetime_field11682(var_args){
var G__11684 = arguments.length;
switch (G__11684) {
case 1:
return sablono.core.datetime_field11682.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_field11682.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.datetime_field11682.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null))].join(''),name__11607__auto__);
});

sablono.core.datetime_field11682.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime","datetime",2135207229,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.datetime_field11682.cljs$lang$maxFixedArity = 2;


sablono.core.datetime_field = sablono.core.wrap_attrs.call(null,sablono.core.datetime_field11682);

/**
 * Creates a datetime-local input field.
 */
sablono.core.datetime_local_field11685 = (function sablono$core$datetime_local_field11685(var_args){
var G__11687 = arguments.length;
switch (G__11687) {
case 1:
return sablono.core.datetime_local_field11685.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.datetime_local_field11685.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.datetime_local_field11685.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null))].join(''),name__11607__auto__);
});

sablono.core.datetime_local_field11685.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"datetime-local","datetime-local",-507312697,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.datetime_local_field11685.cljs$lang$maxFixedArity = 2;


sablono.core.datetime_local_field = sablono.core.wrap_attrs.call(null,sablono.core.datetime_local_field11685);

/**
 * Creates a email input field.
 */
sablono.core.email_field11688 = (function sablono$core$email_field11688(var_args){
var G__11690 = arguments.length;
switch (G__11690) {
case 1:
return sablono.core.email_field11688.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.email_field11688.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.email_field11688.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null))].join(''),name__11607__auto__);
});

sablono.core.email_field11688.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"email","email",-1238619063,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.email_field11688.cljs$lang$maxFixedArity = 2;


sablono.core.email_field = sablono.core.wrap_attrs.call(null,sablono.core.email_field11688);

/**
 * Creates a file input field.
 */
sablono.core.file_field11691 = (function sablono$core$file_field11691(var_args){
var G__11693 = arguments.length;
switch (G__11693) {
case 1:
return sablono.core.file_field11691.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.file_field11691.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.file_field11691.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null))].join(''),name__11607__auto__);
});

sablono.core.file_field11691.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"file","file",370885649,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.file_field11691.cljs$lang$maxFixedArity = 2;


sablono.core.file_field = sablono.core.wrap_attrs.call(null,sablono.core.file_field11691);

/**
 * Creates a hidden input field.
 */
sablono.core.hidden_field11694 = (function sablono$core$hidden_field11694(var_args){
var G__11696 = arguments.length;
switch (G__11696) {
case 1:
return sablono.core.hidden_field11694.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.hidden_field11694.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.hidden_field11694.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null))].join(''),name__11607__auto__);
});

sablono.core.hidden_field11694.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"hidden","hidden",1328025435,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.hidden_field11694.cljs$lang$maxFixedArity = 2;


sablono.core.hidden_field = sablono.core.wrap_attrs.call(null,sablono.core.hidden_field11694);

/**
 * Creates a month input field.
 */
sablono.core.month_field11697 = (function sablono$core$month_field11697(var_args){
var G__11699 = arguments.length;
switch (G__11699) {
case 1:
return sablono.core.month_field11697.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.month_field11697.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.month_field11697.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null))].join(''),name__11607__auto__);
});

sablono.core.month_field11697.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"month","month",-319717006,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.month_field11697.cljs$lang$maxFixedArity = 2;


sablono.core.month_field = sablono.core.wrap_attrs.call(null,sablono.core.month_field11697);

/**
 * Creates a number input field.
 */
sablono.core.number_field11700 = (function sablono$core$number_field11700(var_args){
var G__11702 = arguments.length;
switch (G__11702) {
case 1:
return sablono.core.number_field11700.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.number_field11700.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.number_field11700.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null))].join(''),name__11607__auto__);
});

sablono.core.number_field11700.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"number","number",-1084057331,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.number_field11700.cljs$lang$maxFixedArity = 2;


sablono.core.number_field = sablono.core.wrap_attrs.call(null,sablono.core.number_field11700);

/**
 * Creates a password input field.
 */
sablono.core.password_field11703 = (function sablono$core$password_field11703(var_args){
var G__11705 = arguments.length;
switch (G__11705) {
case 1:
return sablono.core.password_field11703.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.password_field11703.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.password_field11703.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null))].join(''),name__11607__auto__);
});

sablono.core.password_field11703.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"password","password",2057553998,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.password_field11703.cljs$lang$maxFixedArity = 2;


sablono.core.password_field = sablono.core.wrap_attrs.call(null,sablono.core.password_field11703);

/**
 * Creates a range input field.
 */
sablono.core.range_field11706 = (function sablono$core$range_field11706(var_args){
var G__11708 = arguments.length;
switch (G__11708) {
case 1:
return sablono.core.range_field11706.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.range_field11706.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.range_field11706.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null))].join(''),name__11607__auto__);
});

sablono.core.range_field11706.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"range","range",-1014743483,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.range_field11706.cljs$lang$maxFixedArity = 2;


sablono.core.range_field = sablono.core.wrap_attrs.call(null,sablono.core.range_field11706);

/**
 * Creates a search input field.
 */
sablono.core.search_field11709 = (function sablono$core$search_field11709(var_args){
var G__11711 = arguments.length;
switch (G__11711) {
case 1:
return sablono.core.search_field11709.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.search_field11709.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.search_field11709.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null))].join(''),name__11607__auto__);
});

sablono.core.search_field11709.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"search","search",-1089495947,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.search_field11709.cljs$lang$maxFixedArity = 2;


sablono.core.search_field = sablono.core.wrap_attrs.call(null,sablono.core.search_field11709);

/**
 * Creates a tel input field.
 */
sablono.core.tel_field11712 = (function sablono$core$tel_field11712(var_args){
var G__11714 = arguments.length;
switch (G__11714) {
case 1:
return sablono.core.tel_field11712.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.tel_field11712.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.tel_field11712.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null))].join(''),name__11607__auto__);
});

sablono.core.tel_field11712.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"tel","tel",1864669686,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.tel_field11712.cljs$lang$maxFixedArity = 2;


sablono.core.tel_field = sablono.core.wrap_attrs.call(null,sablono.core.tel_field11712);

/**
 * Creates a text input field.
 */
sablono.core.text_field11715 = (function sablono$core$text_field11715(var_args){
var G__11717 = arguments.length;
switch (G__11717) {
case 1:
return sablono.core.text_field11715.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_field11715.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.text_field11715.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null))].join(''),name__11607__auto__);
});

sablono.core.text_field11715.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"text","text",-150030170,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.text_field11715.cljs$lang$maxFixedArity = 2;


sablono.core.text_field = sablono.core.wrap_attrs.call(null,sablono.core.text_field11715);

/**
 * Creates a time input field.
 */
sablono.core.time_field11718 = (function sablono$core$time_field11718(var_args){
var G__11720 = arguments.length;
switch (G__11720) {
case 1:
return sablono.core.time_field11718.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.time_field11718.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.time_field11718.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null))].join(''),name__11607__auto__);
});

sablono.core.time_field11718.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"time","time",-1268547887,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.time_field11718.cljs$lang$maxFixedArity = 2;


sablono.core.time_field = sablono.core.wrap_attrs.call(null,sablono.core.time_field11718);

/**
 * Creates a url input field.
 */
sablono.core.url_field11721 = (function sablono$core$url_field11721(var_args){
var G__11723 = arguments.length;
switch (G__11723) {
case 1:
return sablono.core.url_field11721.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.url_field11721.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.url_field11721.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null))].join(''),name__11607__auto__);
});

sablono.core.url_field11721.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"url","url",1916828573,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.url_field11721.cljs$lang$maxFixedArity = 2;


sablono.core.url_field = sablono.core.wrap_attrs.call(null,sablono.core.url_field11721);

/**
 * Creates a week input field.
 */
sablono.core.week_field11724 = (function sablono$core$week_field11724(var_args){
var G__11726 = arguments.length;
switch (G__11726) {
case 1:
return sablono.core.week_field11724.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.week_field11724.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.week_field11724.cljs$core$IFn$_invoke$arity$1 = (function (name__11607__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null))].join(''),name__11607__auto__);
});

sablono.core.week_field11724.cljs$core$IFn$_invoke$arity$2 = (function (name__11607__auto__,value__11608__auto__){
return sablono.core.input_field_STAR_.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Symbol(null,"week","week",314058249,null))].join(''),name__11607__auto__,value__11608__auto__);
});

sablono.core.week_field11724.cljs$lang$maxFixedArity = 2;


sablono.core.week_field = sablono.core.wrap_attrs.call(null,sablono.core.week_field11724);
sablono.core.file_upload = sablono.core.file_field;
/**
 * Creates a check box.
 */
sablono.core.check_box11744 = (function sablono$core$check_box11744(var_args){
var G__11746 = arguments.length;
switch (G__11746) {
case 1:
return sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$2 = (function (name,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.check_box11744.cljs$core$IFn$_invoke$arity$3 = (function (name,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"checkbox",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.check_box11744.cljs$lang$maxFixedArity = 3;


sablono.core.check_box = sablono.core.wrap_attrs.call(null,sablono.core.check_box11744);
/**
 * Creates a radio button.
 */
sablono.core.radio_button11748 = (function sablono$core$radio_button11748(var_args){
var G__11750 = arguments.length;
switch (G__11750) {
case 1:
return sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$1 = (function (group){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,sablono.util.as_str.call(null,group))], null)], null);
});

sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$2 = (function (group,checked_QMARK_){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,sablono.util.as_str.call(null,group)),new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.radio_button11748.cljs$core$IFn$_invoke$arity$3 = (function (group,checked_QMARK_,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"type","type",1174270348),"radio",new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,group),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,[cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,group)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(sablono.util.as_str.call(null,value))].join('')),new cljs.core.Keyword(null,"value","value",305978217),value,new cljs.core.Keyword(null,"checked","checked",-50955819),checked_QMARK_], null)], null);
});

sablono.core.radio_button11748.cljs$lang$maxFixedArity = 3;


sablono.core.radio_button = sablono.core.wrap_attrs.call(null,sablono.core.radio_button11748);
sablono.core.hash_key = (function sablono$core$hash_key(x){
return goog.string.hashCode(cljs.core.pr_str.call(null,x));
});
/**
 * Creates a seq of option tags from a collection.
 */
sablono.core.select_options11752 = (function sablono$core$select_options11752(coll){
var iter__4324__auto__ = (function sablono$core$select_options11752_$_iter__11753(s__11754){
return (new cljs.core.LazySeq(null,(function (){
var s__11754__$1 = s__11754;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__11754__$1);
if(temp__5457__auto__){
var s__11754__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__11754__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__11754__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__11756 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__11755 = (0);
while(true){
if((i__11755 < size__4323__auto__)){
var x = cljs.core._nth.call(null,c__4322__auto__,i__11755);
cljs.core.chunk_append.call(null,b__11756,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__11757 = x;
var text = cljs.core.nth.call(null,vec__11757,(0),null);
var val = cljs.core.nth.call(null,vec__11757,(1),null);
var disabled_QMARK_ = cljs.core.nth.call(null,vec__11757,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$.call(null,disabled_QMARK_);
if(cljs.core.sequential_QMARK_.call(null,val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),sablono.core.select_options11752.call(null,val)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)));

var G__11763 = (i__11755 + (1));
i__11755 = G__11763;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11756),sablono$core$select_options11752_$_iter__11753.call(null,cljs.core.chunk_rest.call(null,s__11754__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__11756),null);
}
} else {
var x = cljs.core.first.call(null,s__11754__$2);
return cljs.core.cons.call(null,((cljs.core.sequential_QMARK_.call(null,x))?(function (){var vec__11760 = x;
var text = cljs.core.nth.call(null,vec__11760,(0),null);
var val = cljs.core.nth.call(null,vec__11760,(1),null);
var disabled_QMARK_ = cljs.core.nth.call(null,vec__11760,(2),null);
var disabled_QMARK___$1 = cljs.core.boolean$.call(null,disabled_QMARK_);
if(cljs.core.sequential_QMARK_.call(null,val)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"optgroup","optgroup",1738282218),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,text),new cljs.core.Keyword(null,"label","label",1718410804),text], null),sablono.core.select_options11752.call(null,val)], null);
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"disabled","disabled",-1529784218),disabled_QMARK___$1,new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,val),new cljs.core.Keyword(null,"value","value",305978217),val], null),text], null);
}
})():new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"option","option",65132272),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),sablono.core.hash_key.call(null,x),new cljs.core.Keyword(null,"value","value",305978217),x], null),x], null)),sablono$core$select_options11752_$_iter__11753.call(null,cljs.core.rest.call(null,s__11754__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4324__auto__.call(null,coll);
});

sablono.core.select_options = sablono.core.wrap_attrs.call(null,sablono.core.select_options11752);
/**
 * Creates a drop-down box using the <select> tag.
 */
sablono.core.drop_down11764 = (function sablono$core$drop_down11764(var_args){
var G__11766 = arguments.length;
switch (G__11766) {
case 2:
return sablono.core.drop_down11764.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return sablono.core.drop_down11764.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.drop_down11764.cljs$core$IFn$_invoke$arity$2 = (function (name,options){
return sablono.core.drop_down11764.call(null,name,options,null);
});

sablono.core.drop_down11764.cljs$core$IFn$_invoke$arity$3 = (function (name,options,selected){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"select","select",1147833503),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null),sablono.core.select_options.call(null,options,selected)], null);
});

sablono.core.drop_down11764.cljs$lang$maxFixedArity = 3;


sablono.core.drop_down = sablono.core.wrap_attrs.call(null,sablono.core.drop_down11764);
/**
 * Creates a text area element.
 */
sablono.core.text_area11768 = (function sablono$core$text_area11768(var_args){
var G__11770 = arguments.length;
switch (G__11770) {
case 1:
return sablono.core.text_area11768.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return sablono.core.text_area11768.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

sablono.core.text_area11768.cljs$core$IFn$_invoke$arity$1 = (function (name){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name)], null)], null);
});

sablono.core.text_area11768.cljs$core$IFn$_invoke$arity$2 = (function (name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"textarea","textarea",-650375824),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"name","name",1843675177),sablono.core.make_name.call(null,name),new cljs.core.Keyword(null,"id","id",-1388402092),sablono.core.make_id.call(null,name),new cljs.core.Keyword(null,"value","value",305978217),(function (){var or__3949__auto__ = value;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return undefined;
}
})()], null)], null);
});

sablono.core.text_area11768.cljs$lang$maxFixedArity = 2;


sablono.core.text_area = sablono.core.wrap_attrs.call(null,sablono.core.text_area11768);
/**
 * Creates a label for an input field with the supplied name.
 */
sablono.core.label11772 = (function sablono$core$label11772(name,text){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"htmlFor","htmlFor",-1050291720),sablono.core.make_id.call(null,name)], null),text], null);
});

sablono.core.label = sablono.core.wrap_attrs.call(null,sablono.core.label11772);
/**
 * Creates a submit button.
 */
sablono.core.submit_button11773 = (function sablono$core$submit_button11773(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"submit",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.submit_button = sablono.core.wrap_attrs.call(null,sablono.core.submit_button11773);
/**
 * Creates a form reset button.
 */
sablono.core.reset_button11774 = (function sablono$core$reset_button11774(text){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"reset",new cljs.core.Keyword(null,"value","value",305978217),text], null)], null);
});

sablono.core.reset_button = sablono.core.wrap_attrs.call(null,sablono.core.reset_button11774);
/**
 * Create a form that points to a particular method and route.
 *   e.g. (form-to [:put "/post"]
 *       ...)
 */
sablono.core.form_to11775 = (function sablono$core$form_to11775(var_args){
var args__4534__auto__ = [];
var len__4531__auto___11782 = arguments.length;
var i__4532__auto___11783 = (0);
while(true){
if((i__4532__auto___11783 < len__4531__auto___11782)){
args__4534__auto__.push((arguments[i__4532__auto___11783]));

var G__11784 = (i__4532__auto___11783 + (1));
i__4532__auto___11783 = G__11784;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return sablono.core.form_to11775.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

sablono.core.form_to11775.cljs$core$IFn$_invoke$arity$variadic = (function (p__11778,body){
var vec__11779 = p__11778;
var method = cljs.core.nth.call(null,vec__11779,(0),null);
var action = cljs.core.nth.call(null,vec__11779,(1),null);
var method_str = clojure.string.upper_case.call(null,cljs.core.name.call(null,method));
var action_uri = sablono.util.to_uri.call(null,action);
return cljs.core.vec.call(null,cljs.core.concat.call(null,((cljs.core.contains_QMARK_.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"get","get",1683182755),null,new cljs.core.Keyword(null,"post","post",269697687),null], null), null),method))?new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),method_str,new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null)], null):new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"method","method",55703592),"POST",new cljs.core.Keyword(null,"action","action",-811238024),action_uri], null),sablono.core.hidden_field.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),(3735928559)], null),"_method",method_str)], null)),body));
});

sablono.core.form_to11775.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
sablono.core.form_to11775.cljs$lang$applyTo = (function (seq11776){
var G__11777 = cljs.core.first.call(null,seq11776);
var seq11776__$1 = cljs.core.next.call(null,seq11776);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__11777,seq11776__$1);
});


sablono.core.form_to = sablono.core.wrap_attrs.call(null,sablono.core.form_to11775);

//# sourceMappingURL=core.js.map
