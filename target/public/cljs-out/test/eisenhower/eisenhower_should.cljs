(ns eisenhower.eisenhower-should
    (:require
     [cljs.test :refer-macros [deftest is testing]]
      ))


(defn place-in-matrix [what]
  (assoc what :decision "Decide when you will do it"))


(defn expect-decision [expected-decision ent]
  (is (= (assoc ent :decision expect-decision) (place-in-matrix ent))))


(deftest place-a-decision-in-the-matrix
;;   (testing "return-decide-when-for-important-not-urgent"
    (expect-decision
      "Decide when you will do it"
      (-> {}
          (assoc :question "Can I?")
          (assoc :importance 6)
          (assoc :urgency 2)
          (assoc :context "")))
    )
;; )
